# ci-distcheck

Creates tarballs and performs checks.

# The created tarball

The newest created (snapshot) tarball is available through the Gitlab UI
or directly for download: [CI/CD > Jobs > Finished (check-optimized) > Job artifacts > Browse > libsigsegv-snapshot.tar](https://gitlab.com/gnu-libsigsegv/ci-distcheck/-/jobs/artifacts/master/raw/libsigsegv-snapshot.tar?job=check-optimized).
